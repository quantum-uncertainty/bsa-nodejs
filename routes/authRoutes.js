const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        const user = AuthService.login(req.body.email, req.body.password);
        res.data = user;
    } catch (err) {
        res.err = err.toString();
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;