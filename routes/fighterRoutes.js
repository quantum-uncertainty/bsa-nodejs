const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// GET /api/fighters
router.get('/', function (req, res, next) {
    const fighters = FighterService.getAll();
    res.data = fighters;
    next();
});

// GET /api/fighters/:id
router.get('/:id', function (req, res, next) {
    const fighter = FighterService.getById(req.params.id);
    if (fighter) {
        res.data = fighter;
    }
    next();
});

// POST /api/fighters
router.post('/', createFighterValid, function (req, res, next) {
    let fighter = FighterService.create(req.body);
    if (fighter) {
        res.data = fighter;
    } else {
        res.err = 'Cannot create fighter';
    }
    next();
});

// PUT /api/fighters/:id
router.put('/:id', updateFighterValid, function (req, res, next) {
    const id = req.params.id;
    const updateResult = FighterService.update(id, { ...req.body });
    if (updateResult) {
        res.data = updateResult;
    }
    else {
        res.err = `Fighter with id ${id} doesn't exist`;
    }
    next();
});

// DELETE /api/fighters/:id
router.delete('/:id', function (req, res, next) {
    const id = req.params.id;
    if (FighterService.delete(id)) {
        res.data = 'Successfully deleted';
    }
    else {
        res.err =  `Fighter with id ${id} doesn't exist`;
    }
    next();
});

router.use(responseMiddleware);

module.exports = router;