const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// GET /api/users
router.get('/', function (req, res, next) {
    const users = UserService.getAll();
    res.data = users;
    next();
});

// GET /api/users/:id
router.get('/:id', function (req, res, next) {
    const user = UserService.getById(req.params.id);
    if (user) {
        res.data = user;
    }
    next();
});

// POST /api/users
router.post('/', createUserValid, function (req, res, next) {
    let user = UserService.create(req.body);
    if (user) {
        res.data = user;
    } else {
        res.err = 'Cannot create user';
    }
    next();
});

// PUT /api/users/:id
router.put('/:id', updateUserValid, function (req, res, next) {
    const id = req.params.id;
    const updateResult = UserService.update(id, { ...req.body });
    if (updateResult) {
        res.data = updateResult;
    }
    else {
        res.err = `User with id ${id} doesn't exist`;
    }
    next();
});

// DELETE /api/users/:id
router.delete('/:id', function (req, res, next) {
    const id = req.params.id;
    if (UserService.delete(id)) {
        res.data = 'Successfully deleted';
    }
    else {
        res.err =  `User with id ${id} doesn't exist`;
    }
    next();
});

router.use(responseMiddleware);

module.exports = router;