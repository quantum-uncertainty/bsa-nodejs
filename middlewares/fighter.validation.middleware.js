const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    const { error, message } = validateCreate(req.body);
    if (error) {
        res.status(400).json({ error, message });
    }
    else {
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    const { error, message } = validateUpdate(req.body);
    if (error) {
        res.status(400).json({ error, message });
    }
    else {
        next();
    }
}

const { validateCreate, validateUpdate } = (function validate() {
    function error(message) {
        return { error: true, message };
    }

    function validateFields(data) {
        for (let field in data) {
            if (!(field in fighter)) {
                return error(`Field '${field}' is invalid`);
            }
        }

        if ('id' in data) {
            return error('Id is not allowed in the request body');
        }
        if ('power' in data) {
            if (isNaN(data.power)) {
                return error('Power must be a number');
            }
            if (data.power <= 0 || data.power >= 100) {
                return error('Power must be greater than 0 and less than 100');
            }
        }
        if ('defense' in data) {
            if (isNaN(data.defense)) {
                return error('Defense must be a number');
            }
            if (data.defense < 1 || data.defense > 10) {
                return error('Defense must be between 1 and 10');
            }
        }
        return { error: false };
    }

    function validateCreate(data) {
        if (!data.name) {
            return error('Name is required');
        }
        if (!data.power) {
            return error('Power is required');
        }
        if (!data.defense) {
            return error('Defense is required');
        }
        return validateFields(data);
    }

    function validateUpdate(data) {
        return validateFields(data);
    }

    return { validateCreate, validateUpdate };
})();

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;