const responseMiddleware = (req, res) => {
    if (res.err) {
        res.status(400).json({error: true, message: res.err});
    }
    else if(res.data) {
        res.status(200).json(res.data);
    }
    else {
        res.status(404).json(`Sorry, cannot find [${req.method} ${req.originalUrl}]`);
    }
}

exports.responseMiddleware = responseMiddleware;