const { user } = require('../models/user');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
    const { error, message } = validateCreate(req.body);
    if (error) {
        res.status(400).json({ error, message });
    }
    else {
        next();
    }
}

const updateUserValid = (req, res, next) => {
    const { error, message } = validateUpdate(req.body);
    if (error) {
        res.status(400).json({ error, message });
    }
    else {
        next();
    }
}

const { validateCreate, validateUpdate } = (function validate() {
    function error(message) {
        return { error: true, message };
    }

    function validateFields(data) {
        for (let field in data) {
            if (!(field in user)) {
                return error(`Field '${field}' is invalid`);
            }
        }

        const emailRegex = /^.+@gmail\.com$/;
        const phoneRegex = /^\+380\d{9}$/;

        if ('id' in data) {
            return error('Id is not allowed in the request body');
        }
        if ('email' in data) {
            if (!emailRegex.test(data.email)) {
                return error('Only gmail addresses are valid');
            }
            if (UserService.search({ email: data.email })) {
                return error('Email is already used');
            }
        }
        if ('phoneNumber' in data) {
            if (!phoneRegex.test(data.phoneNumber)) {
                return error('Only Ukrainian phone numbers are valid');
            }
            if (UserService.search({ phoneNumber: data.phoneNumber })) {
                return error('Phone number is already used');
            }
        }
        if ('password' in data && data.password.length < 3) {
            return error('Password must be at least 3 characters long');
        }
        return { error: false };
    }

    function validateCreate(data) {
        if (!data.firstName) {
            return error('First name is required');
        }
        if (!data.lastName) {
            return error('Last name is required');
        }
        if (!data.email) {
            return error('Email address is required');
        }
        if (!data.phoneNumber) {
            return error('Phone number is required');
        }
        if (!data.password) {
            return error('Password is required');
        }
        return validateFields(data);
    }

    function validateUpdate(data) {
        return validateFields(data);
    }

    return { validateCreate, validateUpdate };
})();


exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;