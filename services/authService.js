const UserService = require('./userService');

class AuthService {
    login(email, password) {
        const user = UserService.search({ email });
        if (!user) {
            throw Error('User not found');
        }
        if (user.password !== password) {
            throw Error('Wrong password');
        }
        return user;
    }
}

module.exports = new AuthService();