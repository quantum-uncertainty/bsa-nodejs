const { UserRepository } = require('../repositories/userRepository');

class UserService {
    getAll() {
        return UserRepository.getAll();
    }

    getById(id) {
        return this.search({ id });
    }

    create(user) {
        return UserRepository.create(user);
    }

    update(id, data) {
        if (!this.getById(id)) {
            return null;
        }
        return UserRepository.update(id, data);
    }

    delete(id) {
        if (!this.getById(id)) {
            return null;
        }
        return UserRepository.delete(id);
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();