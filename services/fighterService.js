const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getAll() {
        return FighterRepository.getAll();
    }

    getById(id) {
        return this.search({ id });
    }

    create(fighter) {
        fighter.health = 100;
        return FighterRepository.create(fighter);
    }

    update(id, data) {
        if (!this.getById(id)) {
            return null;
        }
        return FighterRepository.update(id, data);
    }

    delete(id) {
        if (!this.getById(id)) {
            return null;
        }
        return FighterRepository.delete(id);
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();